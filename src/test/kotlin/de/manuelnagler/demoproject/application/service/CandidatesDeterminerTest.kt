package de.manuelnagler.demoproject.application.service

import de.manuelnagler.demoproject.application.domain.CandidatesDeterminer
import de.manuelnagler.demoproject.application.port.out.PerformanceReadyDancer
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class CandidatesDeterminerTest {

    @Test
    internal fun noDancersResultInNoDancersForPerformance() {
        withDancers()
            .andLastTrainings(1)
            .noOneWillPerform()
    }

    @Test
    internal fun oneSingleDancerCanNotPerform() {
        withDancers(
            PerformanceReadyDancer(1, "A", true, 10, listOf())
        ).andLastTrainings(1)
            .noOneWillPerform()
    }

    @Test
    internal fun twoDancersCanPerform() {
        withDancers(
            PerformanceReadyDancer(1, "A", true, 10, listOf(1, 2, 3)),
            PerformanceReadyDancer(2, "B", false, 12, listOf(1, 4, 5)),
        ).andLastTrainings(1, 2, 3, 4, 5)
            .theOnesToPerformWillBe(1, 2)
    }

    @Test
    internal fun onlyPairsOfDancersCanPerform() {
        withDancers(
            PerformanceReadyDancer(1, "A", true, 10, listOf(1, 2, 3)),
            PerformanceReadyDancer(2, "B", false, 12, listOf(1, 2, 4)),
            PerformanceReadyDancer(3, "C", false, 14, listOf(1, 3, 5))
        ).andLastTrainings(1, 2, 3, 4, 5)
            .theOnesToPerformWillBe(1, 2)
    }

    @Test
    internal fun dancersWithLessPastPerformancesWillBePreferred() {
        withDancers(
            PerformanceReadyDancer(1, "A", true, 10, listOf(1, 2, 3)),
            PerformanceReadyDancer(2, "B", false, 12, listOf(1, 2, 3)),
            PerformanceReadyDancer(3, "C", false, 8, listOf(1, 2, 3))
        ).andLastTrainings(1, 2, 3, 4, 5)
            .theOnesToPerformWillBe(1, 3)
    }

    @Test
    internal fun dancersWillBePreferredAlphabetically() {
        withDancers(
            PerformanceReadyDancer(1, "A", true, 10, listOf(1, 2, 3)),
            PerformanceReadyDancer(2, "C", false, 10, listOf(1, 2, 3)),
            PerformanceReadyDancer(3, "B", false, 10, listOf(1, 2, 3))
        ).andLastTrainings(1, 2, 3, 4, 5)
            .theOnesToPerformWillBe(1, 3)
    }


    @Test
    internal fun beingPresentInTheLastTrainingBeforeAPerformanceIsMandatory() {
        withDancers(
            PerformanceReadyDancer(1, "A", true, 10, listOf(1, 2, 3, 4, 5)),
            PerformanceReadyDancer(2, "B", false, 10, listOf(2, 3, 4, 5))
        ).andLastTrainings(1, 2, 3, 4, 5)
            .noOneWillPerform()
    }

    @Test
    internal fun beingPresentInAtLeastThreeOfFiveLastTrainingsIsMandatory() {
        withDancers(
            PerformanceReadyDancer(1, "A", true, 10, listOf(1, 2)),
            PerformanceReadyDancer(2, "B", false, 10, listOf(1, 2, 3))
        ).andLastTrainings(1, 2, 3, 4, 5)
            .noOneWillPerform()
    }

    @Test
    internal fun candidatesAreSelectedByPrerequisites() {
        withDancers(
            PerformanceReadyDancer(1, "A", true, 10, listOf(1, 2, 3)),
            PerformanceReadyDancer(2, "B", true, 10, listOf(1, 2)),
            PerformanceReadyDancer(3, "C", false, 10, listOf(1, 2)),
            PerformanceReadyDancer(4, "D", false, 10, listOf(1, 2, 3)),
        ).andLastTrainings(1, 2, 3, 4, 5)
            .theOnesToPerformWillBe(1, 4)
    }

    fun withDancers(vararg dancers: PerformanceReadyDancer) = WithDancers(dancers)

    class WithDancers(private val dancers: Array<out PerformanceReadyDancer>) {
        fun andLastTrainings(vararg lastTrainings: Int): DtAsserter {
            return DtAsserter(getDeterminer(dancers.toList(), lastTrainings.toList()))
        }

        fun getDeterminer(availableDancers: List<PerformanceReadyDancer>, lastTrainings: List<Int>) = CandidatesDeterminer(availableDancers, lastTrainings)
    }

    class DtAsserter(val determiner: CandidatesDeterminer) {
        fun theOnesToPerformWillBe(vararg id: Int) {
            val calculateMembersForPerformance = determiner.calculateMembersForPerformance()
            println(calculateMembersForPerformance)
            assertThat(calculateMembersForPerformance)
                .filteredOn { it.isAllowedToPerform() }
                .flatExtracting({ it.id })
                .containsExactlyInAnyOrder(*id.toTypedArray())
        }

        fun noOneWillPerform() = theOnesToPerformWillBe()
    }

}


