package de.manuelnagler.demoproject.application.service

import de.manuelnagler.demoproject.application.domain.CandidatesDeterminer
import de.manuelnagler.demoproject.application.port.`in`.DetermineCandidatesForPerformanceUseCase
import de.manuelnagler.demoproject.application.port.`in`.MemberForPerformance
import de.manuelnagler.demoproject.application.port.out.LoadLastTrainingsBeforePerformancePort
import de.manuelnagler.demoproject.application.port.out.LoadPerformanceReadyDancersPort
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class DetermineCandidatesForPerformanceService(
    private val loadPerformanceReadyDancersPort: LoadPerformanceReadyDancersPort,
    private val loadLastTrainingsBeforePerformancePort: LoadLastTrainingsBeforePerformancePort
) : DetermineCandidatesForPerformanceUseCase {

    override fun determineCandidatesForPerformance(performanceId: Int): Flux<MemberForPerformance> {
        return Mono.zip(
            loadPerformanceReadyDancersPort.loadPerformanceReadyDancers(performanceId).collectList(),
            loadLastTrainingsBeforePerformancePort.loadLastTrainingsBeforePerformance(performanceId).collectList()
        )
            .map { CandidatesDeterminer(it.t1, it.t2) }
            .flatMapIterable { it.calculateMembersForPerformance() }
    }

}



