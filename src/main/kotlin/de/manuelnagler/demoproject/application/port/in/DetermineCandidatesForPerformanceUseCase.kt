package de.manuelnagler.demoproject.application.port.`in`

import reactor.core.publisher.Flux

@FunctionalInterface
interface DetermineCandidatesForPerformanceUseCase {
    fun determineCandidatesForPerformance(performanceId: Int): Flux<MemberForPerformance>
}

data class MemberForPerformance(
    val id: Int,
    val name: String,
    val reasonsAgainstPerformance: MutableList<ReasonAgainstPerformance>
) {
    fun isAllowedToPerform() = reasonsAgainstPerformance.isEmpty()
}

data class ReasonAgainstPerformance(val type: ReasonType, val details: Any? = null)

enum class ReasonType {
    MISSING_IN_LAST_TRAINING,
    TOO_LITTLE_TRAININGS_PARTICIPATED,
    NO_MORE_PAIRS_POSSIBLE
}
