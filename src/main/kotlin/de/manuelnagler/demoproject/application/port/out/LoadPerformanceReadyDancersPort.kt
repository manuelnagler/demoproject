package de.manuelnagler.demoproject.application.port.out

import reactor.core.publisher.Flux

@FunctionalInterface
interface LoadPerformanceReadyDancersPort {

    fun loadPerformanceReadyDancers(performanceId: Int): Flux<PerformanceReadyDancer>
}

data class PerformanceReadyDancer(
    val id: Int,
    val name: String,
    val dancesMale: Boolean,
    val pastPerformances: Long,
    val visitedTrainings: List<Int>
)
