package de.manuelnagler.demoproject.application.port.out

import reactor.core.publisher.Flux

@FunctionalInterface
interface LoadLastTrainingsBeforePerformancePort {
    fun loadLastTrainingsBeforePerformance(performanceId: Int): Flux<Int>
}
