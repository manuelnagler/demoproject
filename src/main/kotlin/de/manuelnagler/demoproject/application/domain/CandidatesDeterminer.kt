package de.manuelnagler.demoproject.application.domain

import de.manuelnagler.demoproject.application.port.`in`.MemberForPerformance
import de.manuelnagler.demoproject.application.port.out.PerformanceReadyDancer

class CandidatesDeterminer(
    private val membersForPerformance: List<PerformanceReadyDancer>,
    private val lastTrainingsBeforePerformance: List<Int>
) {

    fun calculateMembersForPerformance(): List<MemberForPerformance> {
        val membersForPerformance: List<MemberForPerformanceToDetermine> = membersForPerformance.map { MemberForPerformanceToDetermine(it) }

        PrerequisitesForPerformanceChecker(membersForPerformance, lastTrainingsBeforePerformance).apply {
            checkPrerequisitesForCandidates()
        }

        CandidatesPairBuilder(membersForPerformance).apply {
            buildParsForPerformance()
        }

        return membersForPerformance.map { it.toMemberForPerformance() }
    }

}
