package de.manuelnagler.demoproject.application.domain

import de.manuelnagler.demoproject.application.port.`in`.MemberForPerformance
import de.manuelnagler.demoproject.application.port.`in`.ReasonAgainstPerformance
import de.manuelnagler.demoproject.application.port.out.PerformanceReadyDancer

data class MemberForPerformanceToDetermine(
    val id: Int,
    val name: String,
    val dancesMale: Boolean,
    val pastPerformances: Long,
    val visitedTrainings: List<Int>,
    val reasonAgainstPerformances: MutableList<ReasonAgainstPerformance> = ArrayList()
) {

    constructor(prd: PerformanceReadyDancer) : this(prd.id, prd.name, prd.dancesMale, prd.pastPerformances, prd.visitedTrainings)

    fun isAllowedToPerform() = reasonAgainstPerformances.isEmpty()

    fun toMemberForPerformance() = MemberForPerformance(id, name, reasonAgainstPerformances)
}
