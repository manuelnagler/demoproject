package de.manuelnagler.demoproject.application.domain

import de.manuelnagler.demoproject.application.port.`in`.ReasonAgainstPerformance
import de.manuelnagler.demoproject.application.port.`in`.ReasonType.MISSING_IN_LAST_TRAINING
import de.manuelnagler.demoproject.application.port.`in`.ReasonType.TOO_LITTLE_TRAININGS_PARTICIPATED
import kotlin.math.min


private val requiredParticipationQuote = ParticipationQuote(3, 5)

class PrerequisitesForPerformanceChecker(
    private val membersForPerformance: List<MemberForPerformanceToDetermine>,
    private val lastTrainingsBeforePerformance: List<Int>
) {

    fun checkPrerequisitesForCandidates() {
        requireBeingPresentInTrainings()
        requireBeingPresentInTheTrainingBeforePerformance()
    }

    private fun requireBeingPresentInTrainings() {
        val lastTrainingsToConsider = lastTrainingsBeforePerformance.subList(0, min(requiredParticipationQuote.ofLastTrainings, lastTrainingsBeforePerformance.size))
        membersForPerformance
            .forEach {
                val requiredTrainingsTaken = it.visitedTrainings.intersect(lastTrainingsToConsider).size
                if (requiredTrainingsTaken < requiredParticipationQuote.trainingsToBePresentIn) {
                    it.reasonAgainstPerformances.add(ReasonAgainstPerformance(TOO_LITTLE_TRAININGS_PARTICIPATED, "$requiredTrainingsTaken/${requiredParticipationQuote.trainingsToBePresentIn}"))
                }
            }
    }

    private fun requireBeingPresentInTheTrainingBeforePerformance() {
        if (lastTrainingsBeforePerformance.isEmpty()) {
            return
        }
        membersForPerformance
            .forEach {
                if (!it.visitedTrainings.contains(lastTrainingsBeforePerformance[0])) {
                    it.reasonAgainstPerformances.add(ReasonAgainstPerformance(MISSING_IN_LAST_TRAINING))
                }
            }
    }
}

private data class ParticipationQuote(val trainingsToBePresentIn: Int, val ofLastTrainings: Int)
