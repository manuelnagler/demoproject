package de.manuelnagler.demoproject.application.domain

import de.manuelnagler.demoproject.application.port.`in`.ReasonAgainstPerformance
import de.manuelnagler.demoproject.application.port.`in`.ReasonType.NO_MORE_PAIRS_POSSIBLE
import java.util.Comparator.comparing
import kotlin.math.min

class CandidatesPairBuilder(candidates: List<MemberForPerformanceToDetermine>) {

    private var maleDancers: List<MemberForPerformanceToDetermine>
    private var femaleDancers: List<MemberForPerformanceToDetermine>

    init {
        candidates
            .sortedWith(candidatesPrioritizer)
            .partition { it.dancesMale }
            .also {
                this.maleDancers = it.first
                this.femaleDancers = it.second
            }
    }

    fun buildParsForPerformance() {
        val possiblePairs = getCountOfPossiblePairs()
        maleDancers.addNoMoreParisPossibleReasonToSpareDancers(possiblePairs)
        femaleDancers.addNoMoreParisPossibleReasonToSpareDancers(possiblePairs)
    }

    private fun getCountOfPossiblePairs(): Int = min(
        maleDancers.filter { it.isAllowedToPerform() }.size,
        femaleDancers.filter { it.isAllowedToPerform() }.size
    )
}

private fun List<MemberForPerformanceToDetermine>.addNoMoreParisPossibleReasonToSpareDancers(possiblePairs: Int) = this.asSequence()
    .drop(possiblePairs)
    .filter { it.reasonAgainstPerformances.isEmpty() }
    .forEach { it.reasonAgainstPerformances.add(ReasonAgainstPerformance(NO_MORE_PAIRS_POSSIBLE)) }


private val candidatesPrioritizer =
    comparing { it: MemberForPerformanceToDetermine -> it.reasonAgainstPerformances.size }
        .thenComparing(MemberForPerformanceToDetermine::pastPerformances)
        .thenComparing(MemberForPerformanceToDetermine::name)
