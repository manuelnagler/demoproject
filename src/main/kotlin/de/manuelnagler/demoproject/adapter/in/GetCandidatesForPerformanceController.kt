package de.manuelnagler.demoproject.adapter.`in`

import de.manuelnagler.demoproject.application.port.`in`.DetermineCandidatesForPerformanceUseCase
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux

@RestController
class GetCandidatesForPerformanceController(val determineCandidatesForPerformanceUseCase: DetermineCandidatesForPerformanceUseCase) {

    @GetMapping("/performance/{id}/candidates")
    fun getCandidatesForPerformance(@PathVariable id: Int): Flux<Candidates> {
        return determineCandidatesForPerformanceUseCase.determineCandidatesForPerformance(id)
            .map { Candidates(it.id, it.name, it.reasonsAgainstPerformance.map { "${it.type}(${it.details})" }) }
    }

    data class Candidates(val id: Int, val name: String, val reasonsAgainstPerformance: List<String>)

}
