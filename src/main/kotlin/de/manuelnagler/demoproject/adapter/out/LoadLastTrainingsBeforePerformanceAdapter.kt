package de.manuelnagler.demoproject.adapter.out

import de.manuelnagler.demoproject.application.port.out.LoadLastTrainingsBeforePerformancePort
import org.springframework.r2dbc.core.DatabaseClient
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import java.util.function.Function

@Service
class LoadLastTrainingsBeforePerformanceAdapter(val client: DatabaseClient) : LoadLastTrainingsBeforePerformancePort {
    override fun loadLastTrainingsBeforePerformance(performanceId: Int): Flux<Int> {
        return client.sql(
            """
            select s.id
            from auftritt a
            join stunde s on a.kurs = s.kurs
            where a.id = :performanceId
            and s.datum < a.datum
            order by s.datum desc
            limit 5
            """.trimIndent()
        ).bind("performanceId", performanceId)
            .map(Function { it.get("id") as Int })
            .all()
    }
}
