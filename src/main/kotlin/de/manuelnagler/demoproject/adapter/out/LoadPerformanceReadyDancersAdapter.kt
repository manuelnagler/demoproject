package de.manuelnagler.demoproject.adapter.out

import de.manuelnagler.demoproject.application.port.out.LoadPerformanceReadyDancersPort
import de.manuelnagler.demoproject.application.port.out.PerformanceReadyDancer
import org.springframework.r2dbc.core.DatabaseClient
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.function.Function

@Service
class LoadPerformanceReadyDancersAdapter(val client: DatabaseClient) : LoadPerformanceReadyDancersPort {

    override fun loadPerformanceReadyDancers(performanceId: Int): Flux<PerformanceReadyDancer> {
        return client.sql(
            """
            select m.id, m.kurzname, m.mann, COUNT(a.id) as 'auftritte'
            from mitglied m
                     left join inauftritt i2 on m.id = i2.mitglied
                     left join (SELECT *
                                FROM auftritt a
                                where a.gross = (select a.gross
                                                 from auftritt a
                                                 where a.id = :auftrittId)) a on a.id = i2.auftritt
            where m.auftrittsreif = 1
            group by m.id
        """.trimIndent()
        ).bind("auftrittId", performanceId)
            .map(Function {
                PerformanceReadyDancer(
                    it.get("id") as Int,
                    it.get("kurzname") as String,
                    it.get("mann") as Byte == 1.toByte(),
                    it.get("auftritte") as Long,
                    ArrayList()
                )
            }).all()
            .flatMap { loadTrainings(it) }
    }

    private fun loadTrainings(prd: PerformanceReadyDancer): Mono<PerformanceReadyDancer> {
        return client.sql(
            """
            select i.stunde
            from mitglied
            join instunde i on mitglied.id = i.mitglied
            where mitglied.id = :mitgliedid
        """.trimIndent()
        )
            .bind("mitgliedid", prd.id)
            .map(Function { it.get("stunde") as Int })
            .all().collectList()
            .map { prd.copy(visitedTrainings = it) }
    }
}
