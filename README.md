# About this project

- This project follows a clean architecture approach (Ports&Adapters)
- JPA was not used due to its poor performance for the implemented usecase
- The project is written in Kotlin but should be readable by anyone with java experience
- The database queries are written to work with a preexisting database

## The usecase

- The usecase found in DetermineCandidatesForPerformanceUseCase is part of a bigger project and is extracted for
  showcasing it here
- In order to participate in a performance, a dancer who is considered to be ready to perform (experienced enough), has
  to fulfill some prerequisites in order to be allowed to take part in the next performance. When all prerequisites are
  fullfilled, a dancer might get chosen for the next performance. Dancers are prioritized by the amount of past
  performances followed by the name. Only equal amounts of males and females can perform due to salsa rueda being danced
  in pairs.

## Running this project

- This project exists only for codequality/style and is not planned to be executed (apart from the unittests). Therefore
  no database container is provided
